import itertools
import time
import singer
from singer import write_state
import requests
from datetime import datetime
from tap_test.streams import STREAMS

LOGGER = singer.get_logger()

def write_schema(catalog, stream_name):
    stream = catalog.get_stream(stream_name)
    schema = stream.schema.to_dict()
    try:
        singer.write_schema(stream_name, schema, stream.key_properties)
    except OSError as err:
        LOGGER.info('OS Error writing schema for: {}'.format(stream_name))
        raise err

def sync(config, catalog, state):
    currentDate = str(datetime.now().strftime("%d-%m-%YT%H:%M:%S"))
    value = {"last_updated": currentDate}

    for stream_name, endpoint_config in STREAMS.items():
        record = {"Activity_id": 1, "Activity_name": "Test", "Title": "abc"}
        write_schema(catalog, stream_name)
        singer.write_records(stream_name, [record])
        write_state(value)
        LOGGER.info('[ Finished extracting data ... ]')