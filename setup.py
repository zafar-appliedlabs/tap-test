#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name="tap-test",
    version="1.0.0",
    description="Singer.io tap for extracting data from the test",
    author="Applied Labs",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    py_modules=["tap_test"],
    install_requires=[
        "singer-python>=5.0.12",
        "requests",
    ],
    entry_points='''
    [console_scripts]
    tap-test=tap_test:main
    ''',
    packages=find_packages(),
    package_data={
        'tap_test': [
            'schemas/*.json'
        ]
    })
